# Bitwarden credential deduplication tool

```sh
read MASTER_PASS
bw sync && echo "$MASTER_PASS" | bw export --format json --output bw.json && ipython --pdb dedup.py
```

Or just
```sh
bw sync
python dedup.py
```

Script looks for files created by `bw export --format json` and if it does not find those it'll execute it and create `bw.json`

## Checks

Unique credentials:  
Checks that `username:password` combo is unique accross all entries

# TODO:
- Unique password check
- Unique entry name check
- Check email for compromises from haveibeenpwned
