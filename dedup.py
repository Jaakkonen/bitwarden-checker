import json
from pprint import pformat
from glob import glob
import os
from subprocess import run, check_output

file = next(iter(sorted(glob('bitwarden_export*.json'), reverse=True)), None)
if file is None:
    #run(['bw', 'sync'])
    #run(['bw', 'export', '--format', 'json', '--output', 'bw.json'])
    file = 'bw.json'

data = json.load(open(file))
creds = data['items']
_data_folders = data['folders']
folders = {
    f['id']: f['name']
    for f in _data_folders
}

def duplicate_credentials():
    failed = []
    accs = {}
    for acc in creds:
        try:
            if 'login' not in acc:
                failed.append(acc)
                continue
            login = acc['login']
            print(f'Name: {acc["name"]}')

            _id = f'{login["username"]}:{login["password"]}'
            
            if _id in accs:
                problem = "\n".join((
                    '!!! Duplicate credentials found',
                    '=== Earlier:',
                    pformat(accs[_id]),
                    '=== This',
                    pformat(acc)
                ))
                fix = '\n'.join((
                    'New passwords:',
                    acc['name'],
                    check_output(['bw', 'generate', '-ulns', '--length', '40']).decode('utf-8'),
                    accs[_id]['name'],
                    check_output(['bw', 'generate', '-ulns', '--length', '40']).decode('utf-8')
                ))
                
                yield problem, fix
            else:
                accs[_id] = acc
        except Exception as e:
            print(e)
            raise e
            #print('Encountered problem')
            failed.append(acc)
    if len(failed) != 0:
        print('Failed checks in duplicate_credentials')
        breakpoint()

def delete(id: str):
    run(['bw', 'delete', 'item', id])

def main():
    for p, fix in duplicate_credentials():
        print(p)
        print(fix)
        breakpoint()

if __name__ == "__main__":
    main()
    breakpoint()